$("header").load("site-nav.html")
$(document).ready(function () {

  let url = window.location.href;
  url = url.substring(0, (url.indexOf("?") == -1) ? url.length : url.indexOf("?"))
  let activePage = url;

  $('.links a').each(function () {
      let linkPage = this.href;

      if (activePage == linkPage) {
          $(this).closest("a").addClass("active");
      }
  })
})
